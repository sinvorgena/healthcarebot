import TelegramBot from 'node-telegram-bot-api'

const token = '2029764003:AAF1yt9Pq0wE0he0vX8vN9dARWfaiuMcLsg'

const bot = new TelegramBot(token, {polling: true})
let anyaId = 218560136

let workAcc = 1851468770
let selfAcc = 758553109
let anya = 218560136

import {getDate} from "date-fns"
import {
    checkIsShowAllPressed,
    generateInlineKeyboard,
    generateMessageData, isCheckCurrentDrug, sendGeneratedMessage,
    sendShowAll, setCheckToCurrentDrug,
    writeListToFile
} from "./helpers.js";

setInterval(async () => {
    let currentList = generateMessageData()
    for await (let healthKey of currentList) {
        await sendGeneratedMessage(anya, healthKey, bot)
    }
    if(currentList.length){
        await sendShowAll(anya, bot)
    }
}, 1000)

bot.on('callback_query', async function (data) {
    const messageArr = data.data.split("//")
    const chatId = data.from.id
    const messageId = data.message.message_id
    const currentDrug = messageArr[0]
    const healthKey = messageArr[1]
    const today = getDate(new Date())

    const checkIsShowAll = await checkIsShowAllPressed(data, chatId, bot)
    if (checkIsShowAll) return

    if (isCheckCurrentDrug(healthKey, currentDrug, today)) {
        setCheckToCurrentDrug(healthKey, currentDrug, today)
        bot.editMessageReplyMarkup({
            inline_keyboard: generateInlineKeyboard(healthKey)
        }, {
            chat_id: chatId,
            message_id: messageId,
        });
    }
    writeListToFile()
})